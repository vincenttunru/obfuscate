if (!process.env.CI_JOB_ID) {
  console.error("No CI environment variables detected.");
  process.exit(1);
}

const fs = require("fs");
const manifestJson = require("./manifest.json");
const packageJson = require("./package.json");

if (process.env.CI_COMMIT_TAG) {
  // If we're releasing a stable version, make sure the version is equal in
  // the tag, package.json, and manifest.json
  if (process.env.CI_COMMIT_TAG !== `v${packageJson.version}`) {
    console.error('Version is package.json is not equal to the tag name');
    process.exit(1);
  }
  manifestJson.version = packageJson.version;
} else {
  // If we're releasing an unstable version, make sure its version number
  // does not match the published stable version. Each number must be up to 9
  // digits in length though, per addons.mozilla.org policy.
  const prereleaseVersion = `${manifestJson.version}.${process.env.CI_JOB_ID.substring(process.env.CI_JOB_ID.length - 9)}`;
  console.log(`Setting pre-release version: [${prereleaseVersion}]`)
  manifestJson.version = prereleaseVersion;
  packageJson.version = prereleaseVersion;
}

fs.writeFileSync("./manifest.json", JSON.stringify(manifestJson));
fs.writeFileSync("./package.json", JSON.stringify(packageJson));
